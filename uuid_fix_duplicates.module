<?php

/**
 * @file
 * Helper module to review and fix duplicated UUID in the system.
 *
 * Work in progress, currently done for the nodes only.
 *
 * Created because of d.o 2235947 replicate issue.
 * @see https://www.drupal.org/node/2235947
 */

/**
 * Implements hook_menu().
 */
function uuid_fix_duplicates_menu() {
  // Admin screen to fix UUIDs (if needed).
  $items['admin/config/system/uuid-fix-duplicates'] = array(
    'type' => MENU_NORMAL_ITEM,
    'title' => 'Fix duplicated UUIDs',
    'description' => 'Regrenerate non-unique UUIDs, if accidentally created by replicate module.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('uuid_fix_duplicates_admin_form'),
    'access arguments' => array('administer uuid'),
  );
  return $items;
}


/**
 * Form builder; Regrenerate non-unique UUIDs, if needed.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function uuid_fix_duplicates_admin_form() {
  $form['issue_description'] = array(
    '#markup' => t('When replicating an entity using the replicate module (7.x-1.1 or lower), the UUID will be duplicated, see !issue.',
      array('!issue' => l(t('d.o. issue'), 'https://www.drupal.org/node/2235947')))
  );
  $dulicated_node_uuids = _uuid_fix_dulicates_get_node_uuid_dups();
  $update_needed = !empty($dulicated_node_uuids);
  if ($update_needed) {
    $form['node_uuid'] = array(
      '#type' => 'fieldset',
      '#title' => 'Duplicated Node UUIDs',
      '#description' => t('There are @count non-unique node UUIDs in the system.', array('@count' => count($dulicated_node_uuids))),
    );
    $vars = array(
      'items' => $dulicated_node_uuids,
      'type' => 'ol',
      'attributes' => array()
    );
    $form['node_uuid']['list'] = array(
      '#markup' => theme('item_list', $vars),
    );
    $form['node_uuid']['more_info'] = array(
      '#type' => 'fieldset',
      '#title' => 'More info',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    foreach($dulicated_node_uuids as $uuid) {
      $dulicates = _uuid_fix_dulicates_get_node_uuid_dups_info($uuid);
      $vars = array(
        'items' => $dulicates,
        'title' => $uuid,
        'type' => 'ol',
        'attributes' => array()
      );
      $form['node_uuid']['more_info'][$uuid] = array(
        '#markup' => theme('item_list', $vars),
      );
    }
    $form['node_uuid']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Regrenerate UUIDs for nodes'),
      '#suffix' => t('This will regenerate dulicated UUIDs of the 2+ nodes. Note, that UUID original node (node with the lowest NID per UUID) will be unchanged.'),
      '#submit' => array('uuid_fix_duplicates_admin_form_node_submit'),
    );
  }
  else {
    drupal_set_message(t('No update required. All node UUIDs are unique in the system.'));
  }
  return $form;
}

/**
 * Helper function; Get list of non-unique UUID for the nodes.
 */
function _uuid_fix_dulicates_get_node_uuid_dups() {
  // Query if there are dulicate UUIDs in node table.
  // TODO - rewrite to db_select().
  return db_query('SELECT DISTINCT n.uuid
FROM {node} AS n
INNER JOIN {node} AS n2
ON n2.uuid = n.uuid AND n2.nid <> n.nid')->fetchAllKeyed(0, 0);
}

/**
 * Helper function; Get short node info about the duplicated nodes.
 */
function _uuid_fix_dulicates_get_node_uuid_dups_info($uuid) {
  // Load short node info by duplicated UUID.
  // TODO - rewrite to db_select().
  $records = db_query('SELECT n.nid, n.type, n.title, FROM_UNIXTIME(n.created) as created
FROM {node} AS n
WHERE uuid = :uuid
ORDER BY n.nid', array(':uuid' => $uuid))->fetchAll();
  $dulicates = array();
  foreach ($records as $n) {
    // Output node title (linked to the node page), nid, type and created date.
    $dulicates[] = l($n->title, 'node/' . $n->nid) . " (nid: {$n->nid}, type: {$n->type}, created: {$n->created})";
  }
  return $dulicates;
}

/**
 * Form submit; Regenerate UUID of the nodes (if duplicated).
 */
function uuid_fix_duplicates_admin_form_node_submit() {
  // Load all node with duplicated UUID.
  $uuids = _uuid_fix_dulicates_get_node_uuid_dups();
  $query = db_select('node', 'n');
  $query->fields('n', array('nid', 'uuid', 'type', 'title'));
  $query->addExpression('FROM_UNIXTIME(n.created)', 'created');
  $query->condition('n.uuid', $uuids, 'IN');
  $records = $query->execute()->fetchAll();

  // Separate origin and dulicates.
  $dulicates = array();
  $origins = array();
  foreach ($records as $n) {
    // Keep very 1st node (origin) UUID unchanged.
    if (empty($origins[$n->uuid])) {
      $origins[$n->uuid] = $n;
    }
    // Mark other as duplicates (to be updated).
    else {
      $dulicates[$n->nid] = $n->nid;
    }
  }

  // Origin nodes will not be modified. Inform user about this.
  drupal_set_message('Origins nodes: ' . count($origins));
  foreach ($origins as $n) {
    $message = "Not modified: ". l($n->title, 'node/' . $n->nid) . " (nid: {$n->nid}, type: {$n->type}, created: {$n->created})";
    drupal_set_message($message);
  }

  // Re-generate UUIDs for duplicates.
  drupal_set_message('Dulicates nodes: ' . count($dulicates));
  module_load_include('inc', 'uuid', 'uuid');
  $nodes = node_load_multiple($dulicates);
  $updated = 0;
  foreach ($nodes as $node) {
    // Create new revision. Just to be safe to track the change.
    $node->uuid = uuid_generate();
    $node->vuuid = uuid_generate();
    $node->log = 'Re-generate UUID for the replicated nodes.';
    $node->revision = 1;
    node_save($node);
    $updated++;

    // Inform user.
    $message = "UUID has been updated for: " . l($node->title, 'node/' . $node->nid) . " (nid: {$node->nid}, type: {$node->type}, created: " . date("c", $node->created);
    drupal_set_message($message);
    watchdog('replicate_uuid', $message);
  }

  // Completed.
  drupal_set_message(t('Completed. UUID has been updated for @updated nodes.', array('@updated' => $updated)));
}